package tochukwu.chikeluba.drones.enums;

public enum Model {
    Lightweight,
    Middleweight,
    Cruiserweight,
    Heavyweight
}
