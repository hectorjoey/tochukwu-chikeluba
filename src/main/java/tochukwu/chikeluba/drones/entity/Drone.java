package tochukwu.chikeluba.drones.entity;


import tochukwu.chikeluba.drones.enums.Model;
import tochukwu.chikeluba.drones.enums.State;

import javax.persistence.*;

@Entity
@Table(name = "drones")
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String weight;
    private String battery;
    private Model Model;
    private State state;


    public Drone() {
    }

    public Drone(String weight, String battery, tochukwu.chikeluba.drones.enums.Model model, State state) {
        this.weight = weight;
        this.battery = battery;
        Model = model;
        this.state = state;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public tochukwu.chikeluba.drones.enums.Model getModel() {
        return Model;
    }

    public void setModel(tochukwu.chikeluba.drones.enums.Model model) {
        Model = model;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
