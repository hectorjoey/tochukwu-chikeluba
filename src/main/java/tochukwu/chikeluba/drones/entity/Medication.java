package tochukwu.chikeluba.drones.entity;


import javax.persistence.*;

@Entity
@Table(name = "medications")
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name; //(allowed only letters, numbers, ‘-‘, ‘_’);
    private String weight;
    private String code;  //(allowed only upper case letters, underscore and numbers);
//    private String image;   //(picture of the medication case).

    private long droneId;

    public Medication() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getDroneId() {
        return droneId;
    }

    public void setDroneId(long droneId) {
        this.droneId = droneId;
    }
}
